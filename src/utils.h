#ifndef _UTILS_H
#define _UTILS_H

/***************************************************************************************/
/***************************************************************************************/

void print_s (int * const s );

double magnetization ( int * const s );

void init ();
void fini ();

int init_nn_fields(void);

void apply_K ( double * const r, double * const s );
void apply_J ( double * const r, double * const s );
void apply_diag ( double * const r, double * const s );

void op_mat ( double * const r, apply_op_type * const op, double * const s, double const dt, double const sign, unsigned int const l, double ** const aux );

void run_eom (double * const x_new, double * const p_new, double * const x_old, double * const p_old, double const t, double ** const aux );

double phi_energy (double * const x, double * const p, double ** const aux );

double phi_energy_2 (double * const x, double * const p, double ** const aux );

double phi_magnetization ( double * const x );

double phi_weight (double * const x , double ** const aux);

void extreme_eval ( double * const lambda, double ** const aux );

void phi_force ( double * const f, double * const x, double ** const aux );

void phi_force_2 ( double * const f, double * const x );

double phi_abs ( double * const x );

double phi_sign ( double * const x );

double phi_square ( double * const x );

#endif
