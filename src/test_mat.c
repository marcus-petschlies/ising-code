#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#include "ranlxd.h"

#ifdef HAVE_OPENMP
#include <omp.h>
#endif

#define MAIN_PROGRAM
#include "global.h"
#include "table_init_d.h"
#include "utils.h"

/***************************************************************************************
 *
 ***************************************************************************************/


int main(int argc, char **argv) {

  int c;
  int seed = 0;
  char LL_str[200] = "NA";

  init();

  while ((c = getopt(argc, argv, "h:N:J:s:L:d:v:m:t:")) != -1) {
    switch (c) {
      case 'd':
        dim = atoi(optarg);
        fprintf(stdout, "# [test_mat] dimension set to %u\n", dim );
        break;
      case 'J':
        J = atof(optarg);
        fprintf(stdout, "# [test_mat] interaction energy J set to %f\n", J);
        break;
      case 'm':
        mass = atof(optarg);
        fprintf(stdout, "# [test_mat] mass set to %f\n", mass );
        break;
      case 'L':
        strcpy( LL_str, optarg);
        fprintf(stdout, "# [test_mat] lattice size string set to %s\n", LL_str);
        break;
      case 's':
        seed = atoi(optarg);
        fprintf(stdout, "# [test_mat] seed = %i\n", seed );
        break;
      case 't':
        num_threads = atoi(optarg);
        fprintf(stdout, "# [test_mat] num_threads = %i\n", num_threads );
        break;
      case 'v':
        verbose = atoi(optarg);
        fprintf(stdout, "# [test_mat] verbosity level = %d\n", verbose );
        break;
      default:
        exit(1);
    }
  }

  /******************************************************
   * report git version
   ******************************************************/
  fprintf(stdout, "# [test_mat] git version = %s\n", gitversion);

  /******************************************************
   * initialize random number generator
   ******************************************************/
  rlxd_init ( 2, seed);

  /******************************************************
   * check dim
   ******************************************************/
  if ( dim == 0 ) {
    fprintf ( stderr, "[test_mat] Error, need dim\n" );
    exit(1);
  }

  /******************************************************
   * check omp setup
   ******************************************************/
#ifdef HAVE_OPENMP
  omp_set_num_threads ( num_threads );
#pragma omp parallel
{
  fprintf ( stdout, "# [test_mat] thread no. %2d reports number of threads %d\n", omp_get_thread_num(), omp_get_num_threads () );
}
#endif


  /******************************************************
   * check lattice size string
   ******************************************************/
  if ( strcmp( LL_str, "NA" ) == 0 ) {
    fprintf ( stderr, "[test_mat] Error, LL string\n" );
    exit(2);
  }

  /******************************************************
   * set lattice dimensions
   ******************************************************/
  LL = (unsigned int *)calloc ( dim, sizeof(unsigned int ) );
  char * ptr = strtok ( LL_str, "," );
  if ( ptr == NULL ) exit(3);
  LL[0] = atoi ( ptr );
  for ( unsigned int i = 1; i < dim; i++ ) {
    ptr = strtok ( NULL, "," );
    if ( ptr == NULL ) exit(3);
    LL[i] = atoi ( ptr );
  }

  for ( unsigned int i = 0; i < dim; i++ ) {
    fprintf ( stdout, "# [test_mat] LL[%u] = %4u\n", i, LL[i] );
  }

  /******************************************************
   * set VOLUME
   ******************************************************/
  VOLUME = 1;
  for ( unsigned int i = 0; i < dim; i++ ) {
    VOLUME *= LL[i];
  }
  fprintf ( stdout, "# [test_mat] VOLUME = %lld\n", VOLUME );


  /******************************************************
   * init nearest neighbour indices
   ******************************************************/
  init_nn_fields ();

  /******************************************************
   * allocate spin field
   ******************************************************/

  double ** aux = init_2level_dtable ( num_aux_field, VOLUME );
  double ** s   = init_2level_dtable ( 2, VOLUME );

  if( s == NULL || aux == NULL ) {
    fprintf(stderr, "[test_mat] Error from init_2level_dtable\n");
    exit(4);
  }

  /* output */

  fprintf( stdout, "# J      = %f\n", J );
  fprintf( stdout, "# dim    = %u\n", dim );
  for( unsigned int i = 0; i < dim; i++ ) {
    fprintf( stdout, "# L %u = %u\n", i, LL[i] );
  }
  fprintf( stdout, "# seed   = %d\n", seed );

  unsigned int * coords = (unsigned int *) malloc ( dim * sizeof(unsigned int) );
  if ( coords == NULL ) {
    fprintf ( stderr, "[test_mat] Error from malloc\n");
    exit(2);
  }

#if 0
  /******************************************************
   * test apply_K
   ******************************************************/
  rangauss( s[0], VOLUME );
  apply_K ( s[1], s[0] );
  for ( long long int i = 0; i < VOLUME ; i++ ) {
    fprintf ( stdout, "%8lld", i );
    lexic2coords(coords, i);
    for ( unsigned int k = 0; k < dim; k++ ) {
      fprintf ( stdout, "%4d", coords[k]);
    }
    fprintf ( stdout, "%25.16e %25.16e\n", s[0][i], s[1][i] );
  }
#endif
  
  /******************************************************
   * test op_mat
   ******************************************************/
#if 0
  double const dt = 1.7;
  rangauss( s[0], VOLUME );
  /* op_mat ( s[1], apply_diag, s[0], dt, 0, aux ); */
  op_mat ( s[1], apply_diag, s[0], dt, 1, aux );
  for ( long long int i = 0; i < VOLUME ; i++ ) {
    fprintf ( stdout, "%8lld", i );
    lexic2coords(coords, i);
    for ( unsigned int k = 0; k < dim; k++ ) {
      fprintf ( stdout, "%4d", coords[k]);
    }
    fprintf ( stdout, "%25.16e %25.16e\n", s[0][i], s[1][i] );
  }
#endif

#if 0
  double ** p = init_2level_dtable ( 2, VOLUME );
  if( p == NULL ) {
    fprintf(stderr, "[test_mat] Error from init_2level_dtable\n");
    exit(4);
  }

  rangauss( s[0], VOLUME );
  rangauss( p[0], VOLUME );
#endif

#if 0
  double sp = 0., sp2 = 0.;
  re_eq_v_dot_v ( &sp, s[0], p[0]);
  for ( long long int i = 0; i < VOLUME; i++ ) { sp2 += s[0][i] * p[0][i]; }
  fprintf ( stdout, "# [test_mat] %25.16e %25.16e %25.16e\n", sp, sp2, 2.*(sp-sp2)/(sp+sp2) );
#endif

#if 0
  double H_old = phi_energy ( s[0], p[0], aux );
  double dt = 1.7;
  run_eom ( s[1], p[1], s[0], p[0], dt, aux );
  double H_new = phi_energy ( s[1], p[1], aux );
  fprintf ( stdout, "# [test_mat] dt = %e   H_old = %25.16e    H_new = %25.16e    dH/H = %16.7e\n", dt, H_old, H_new , 2*(H_new - H_old)/(H_new + H_old) );
  double V_old, V_new, T_old, T_new;
  apply_K ( aux[0], s[0] );
  re_eq_v_dot_v ( &V_old, s[0], aux[0]);
  apply_K ( aux[0], s[1] );
  re_eq_v_dot_v ( &V_new, s[1], aux[0]);
  re_eq_v_dot_v ( &T_old, p[0], p[0]);
  re_eq_v_dot_v ( &T_new, p[1], p[1]);
  fprintf ( stdout, "# [test_mat] V_old %25.16e   T_old %25.16e    V_new %25.16e   T_new %25.16e\n", V_old, T_old, V_new, T_new );
#endif

#if 0
  rangauss( s[0], VOLUME );
  double dt = 1.7;
  update ( s[0] , dt, aux );
#endif

#if 0
  rangauss( s[0], VOLUME );
  double const magn = phi_magnetization ( s[0], aux );
  double const w    = phi_weight ( s[0], aux );
  double magn2 = 0.;
  double w2    = 1.;
  apply_K ( aux[0], s[0] );
  for ( long long int i = 0; i < VOLUME; i++ ) {
    magn2 += tanh( aux[0][i] );
    w2 *= cosh( aux[0][i] );
  }
  fprintf ( stdout, "# [test_mat] magn   %25.16e   magn2   %25.16e    diff %16.7e\n", magn, magn2, 2.*(magn2-magn)/(magn + magn2) );
  fprintf ( stdout, "# [test_mat] weight %25.16e   weight2 %25.16e    diff %16.7e\n", w, w2, 2.*(w2-w)/(w + w2) );
#endif

#if 0
  int const Np = 100000;
  double * p = init_1level_dtable ( Np ); 
  rangauss( p, Np );
  for ( int i = 0; i < Np; i++ ) fprintf ( stdout, "  %25.16e\n", p[i] );
  fini_1level_dtable ( &p );
#endif

#if 0
  double lambda[2];
  extreme_eval ( lambda, aux );
#endif

  /******************************************************
   * deallocate
   ******************************************************/
  fini_2level_dtable ( &s );
#if 0
  fini_2level_dtable ( &p );
#endif
  fini_2level_dtable ( &aux );
  free ( coords );

  fini();
  return(0);
}
