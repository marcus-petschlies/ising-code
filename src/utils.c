#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>

#include "table_init_d.h"
#include "global.h"
#include "geometry.h"
#include "linalg.h"
#include "random.h"
#include "utils.h"

/***************************************************************************************
 *
 ***************************************************************************************/

void print_s ( int * const s ) { 

  if ( dim == 2 ) {
    long long int ik = 0;
    for ( unsigned int i = 0; i < LL[0]; i++ ) {

      for ( unsigned int k = 0; k < LL[1]; k++ ) {

        if( s[ik] == 1 )  {
          fprintf(stdout, "x|");
        } else {
          fprintf(stdout, "o|");
        }

        ik++;
      }
      fprintf(stdout, "\n");
      for ( unsigned int k = 0; k < 2*LL[1]; k++ ) {
        fprintf(stdout, "-");
      }
      fprintf(stdout, "\n");
    }
    fprintf(stdout, "\n");
  }

}  /* end of print_s */

/***************************************************************************************
 *
 ***************************************************************************************/
void print_formatR (double * const x, long long int N , char * const name, FILE * const ofs ) {

  fprintf ( ofs, "%s <- numeric(%lld)\n", name, N );

  for ( long long int i = 0; i < N; i++ ) {
    fprintf ( ofs, "%s[%lld] <- %25.16e\n", name, i+1, x[i] );
  }
}

/***************************************************************************************
 *
 ***************************************************************************************/
double magnetization ( int * const s ) {
  double res = 0.;
  for ( long long int i = 0; i < VOLUME; i++ ) {
    res += s[i];
  }
  return ( res / (double)VOLUME );
}  /* end of magnetization */

/***************************************************************************************
 *
 ***************************************************************************************/
void init () {
  verbose = 0;
  dim     = 0;
  LL      = NULL;
  LLconv  = NULL;
  VOLUME  = 0;
  id_up   = NULL;
  id_dn   = NULL;
  g_op    = apply_K;
  num_aux_field = 8;
  hext    = NULL;
#ifdef HAVE_OPENMP
  num_threads = omp_get_max_threads();
#else
  num_threads = 1;
#endif
}  /* end of init */

/***************************************************************************************
 *
 ***************************************************************************************/
void fini () {
  if ( id_up != NULL ) {
    if ( id_up[0] != NULL ) free ( id_up[0] );
    free ( id_up );
  }
  if ( id_dn != NULL ) {
    if ( id_dn[0] != NULL ) free ( id_dn[0] );
    free ( id_dn );
  }
  if ( LL != NULL ) free ( LL );
  if ( LLconv != NULL ) free ( LLconv );

  if ( hext != NULL) free ( hext );

}  /* end of fini */

/***************************************************************************************
 *
 ***************************************************************************************/
int init_nn_fields (void) {

  id_up = ( long long int ** ) malloc ( VOLUME * sizeof( long long int * ) );
  id_up[0] = ( long long int * ) malloc ( dim * VOLUME * sizeof( long long int ) );
  for ( long long int i = 1; i < VOLUME; i++ ) id_up[i] = id_up[i-1] + dim;

  id_dn = ( long long int ** ) malloc ( VOLUME * sizeof( long long int * ) );
  id_dn[0] = ( long long int * ) malloc ( dim * VOLUME * sizeof( long long int ) );
  for ( long long int i = 1; i < VOLUME; i++ ) id_dn[i] = id_dn[i-1] + dim;

  unsigned int * coords = (unsigned int*)malloc ( dim * sizeof(unsigned int) );
  unsigned int * shift  = (unsigned int*)malloc ( dim * sizeof(unsigned int) );

  LLconv = (long long int*)malloc ( dim * sizeof(long long int) );

  LLconv[dim-1] = 1;
  if ( dim >= 2 ) {
    for ( unsigned int i = dim-2; ; i-- ) {
      LLconv[i] = LLconv[i+1] * LL[i+1];
      if ( i == 0 ) break;
    }
  }
  for ( unsigned int i = 0; i < dim; i++ ) {
    fprintf ( stdout, "# [init_nn_fields] LLconv[%u] = %lld\n", i, LLconv[i] );
  }


  for ( long long int x = 0; x < VOLUME; x++ ) {

    coords[0] = x / LLconv[0];
    for ( unsigned int i = 1; i < dim; i++ ) {
      coords[i] = ( x % LLconv[i-1] ) / LLconv[i];
    }

    if ( verbose > 3 ) {
      fprintf ( stdout, "# [init_nn_fields] coords " );
      for ( unsigned int k = 0; k < dim; k++ ) fprintf ( stdout, " %4d", coords[k] );
      fprintf ( stdout, "\n" );
    }
    for ( unsigned int i = 0; i < dim; i++ ) {
      memset ( shift, 0, dim*sizeof(unsigned int) );
      shift[i] = 1;
      long long int x_up = 0;
      for ( unsigned int k = 0; k < dim; k++ ) {
        x_up += ( ( coords[k] + shift[k] + LL[k] ) % LL[k] ) * LLconv[k];
      }
      id_up[x][i] = x_up;
      long long int x_dn = 0;
      for ( unsigned int k = 0; k < dim; k++ ) {
        x_dn += ( ( coords[k] - shift[k] + LL[k] ) % LL[k] ) * LLconv[k];
      }
      id_dn[x][i] = x_dn;

      if ( verbose > 3 ) fprintf ( stdout, "  x %8lld dim %u up %8lld dn %8lld\n", x, i, x_up, x_dn );
    }  /* end of loop on dimensions */

  }  /* end of loop on VOLUME */

  free ( coords );
  free ( shift );

  return (0);
}  /* init_nn_fields */

/***************************************************************************************
 *
 ***************************************************************************************/
void apply_K ( double * const r, double * const s ) {

#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    r[i] = mass * s[i];
    double dtmp = 0.;
    for ( unsigned int d = 0;  d < dim; d++ ) {
      dtmp += s[id_up[i][d]] + s[id_dn[i][d]];
    }
    r[i] += J * dtmp;
  }
}  /* end of apply_K */

/***************************************************************************************
 *
 ***************************************************************************************/
void apply_J ( double * const r, double * const s ) {

#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    double dtmp = 0.;
    for ( unsigned int d = 0;  d < dim; d++ ) {
      dtmp += s[id_up[i][d]] + s[id_dn[i][d]];
    }
    r[i] += J * dtmp;
  }
}  /* end of apply_J */


/***************************************************************************************
 *
 ***************************************************************************************/
void apply_diag ( double * const r, double * const s ) {

#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    r[i] = tanh ( (double)i ) * s[i];
  }
}

/***************************************************************************************
 *
 ***************************************************************************************/
void op_mat ( double * const r, apply_op_type * const op, double * const s, double const dt, double const sign, unsigned int const l, double ** const aux ) {

  size_t const bytes = VOLUME*sizeof(double);
  double const dt2 = dt * dt;
  int const lm1 = l - 1;

  /* assign r <- s */
  memcpy ( r, s, bytes );
  
  double * p = aux[0];
  double * q = aux[1];
  double * p_swap;

  /* assign q <- s */
  memcpy ( q, s, bytes );
  memset ( p, 0, bytes );

  double coeff = 1.;

  for ( unsigned int i = 1; i <= MAX_TRIG_ORDER; i++ ) {
    /* p <- op ( q ) */
    op ( p , q );

    coeff *= sign * dt2/ (double)( (2*i+lm1) * (2*i+l) );

    v_pl_eq_v_ti_re ( r, p, coeff );

    /* swap q and p,  q <-> p */
    p_swap = p;
    p = q;
    q = p_swap;

  }

  if ( l == 1 ) {
    v_ti_eq_re ( r, dt );
  }

}  /* end of op_mat */

/***************************************************************************************
 *
 ***************************************************************************************/
void run_eom (double * const x_new, double * const p_new, double * const x_old, double * const p_old, double const t, double ** const aux ) {

  /* rotate x */
  op_mat ( x_new,  g_op, x_old, t, -1, 0 , aux );
  op_mat ( aux[0], g_op, p_old, t, -1, 1, &(aux[1]) );
  v_pl_eq_v ( x_new, aux[0] );

  /* rotate p */
  g_op( aux[0], x_old );
  op_mat ( p_new,  g_op, p_old,  t, -1, 0, &(aux[1]) );
  op_mat ( aux[1], g_op, aux[0], t, -1, 1, &(aux[2]) );
  v_mi_eq_v ( p_new, aux[1] );

}  /* end of run_eom */

/***************************************************************************************
 * H =  p^2/2 + x K x - log ( cosh ( K x ) ) + x hext
 ***************************************************************************************/
double phi_energy (double * const x, double * const p, double ** const aux ) {

  /* aux[0] <- K x */
  g_op ( aux[0], x );

  /* res1 <- x K x / 2 */
  double res1 = 0.;
  re_eq_v_dot_v ( &res1, x, aux[0]);
  res1 *= 0.5;

  /* res1 <- res1 - log( cosh( aux[0] ) ) */
#ifdef HAVE_OPENMP
  omp_lock_t writelock;
  omp_init_lock(&writelock);
#pragma omp parallel
{
#endif
  double res_tmp=0.;
#pragma omp for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    res_tmp -= log( cosh( aux[0][i] ) ) + hext[i] * x[i];
  }
#ifdef HAVE_OPENMP
  omp_set_lock(&writelock);
#endif
  res1 += res_tmp;
#ifdef HAVE_OPENMP
   omp_unset_lock(&writelock);
}  /* end of parallel region */
  omp_destroy_lock(&writelock);
#endif

  double res2 = 0.;
  re_eq_v_dot_v ( &res2, p, p);
  res2 *= 0.5;

  return ( res1 + res2 );
}  /* end of phi_energy */

/***************************************************************************************
 *
 ***************************************************************************************/
double phi_magnetization ( double * const x ) {

  double magn = 0.;

#ifdef HAVE_OPENMP
  omp_lock_t writelock;
  omp_init_lock(&writelock);
#pragma omp parallel default(shared)
{
#endif

  double magn2 = 0.;
#pragma omp for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    magn2 += x[i];
  }

  #ifdef HAVE_OPENMP
  omp_set_lock(&writelock);
#endif
  magn += magn2;
#ifdef HAVE_OPENMP
  omp_unset_lock(&writelock);
}  /* end of parallel region */
  omp_destroy_lock(&writelock);
#endif

  return( magn );
}  /* end of phi_magnetization_and_weight */

/***************************************************************************************
 *
 ***************************************************************************************/
double phi_weight (double * const x , double ** const aux) {

  double w = 1.;
  double * const y = aux[0];
  g_op ( y, x );

#ifdef HAVE_OPENMP
  omp_lock_t writelock;
  omp_init_lock(&writelock);

#pragma omp parallel default(shared)
{
#endif

  double w2 = 1.;
#pragma omp for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    w2 *= cosh ( y[i] );
  }
#ifdef HAVE_OPENMP
  omp_set_lock(&writelock);
#endif
  w *= w2;
#ifdef HAVE_OPENMP
  omp_unset_lock(&writelock);
}  /* end of parallel region */
  omp_destroy_lock(&writelock);
#endif

  return ( w );
}  /* end of phi_weight */

/***************************************************************************************
 *
 ***************************************************************************************/
void extreme_eval ( double * const lambda, double ** const aux ) {

  double * s = aux[0];
  double * v = aux[1];
  double * w = aux[2];
  double dtmp[2] = {0., 0.};

  /***************************************************************************************
   * largest eval of K
   ***************************************************************************************/
  rangauss ( s, VOLUME );
  re_eq_v_dot_v ( &(dtmp[1]), s, s );
  v_eq_v ( v, s );
  fprintf ( stdout, "# [extreme_eval] lambda_max\n" );
  for ( int i = 1; i <= MAX_GEOM_ORDER; i++ ) {
    g_op ( w, v );
    double * swap = v;
    v = w;
    w = swap;
    /* check evec property of current v */
    g_op ( w, v );
    re_eq_v_dot_v ( &(dtmp[0]), v, w );
    re_eq_v_dot_v ( &(dtmp[1]), v, v );
    fprintf ( stdout, "    %4d   %25.16e %25.16e    %25.16e\n", i, dtmp[0], dtmp[1],  dtmp[0]/dtmp[1] );
  }

  /* final evaluation */
  g_op ( w, v );
  re_eq_v_dot_v ( &(dtmp[0]), v, w );
  re_eq_v_dot_v ( &(dtmp[1]), v, v );

  lambda[1] = dtmp[0]/dtmp[1];
  fprintf ( stdout, "# [extreme_eval] lambda_max = %25.16e\n", lambda[1] );

  /***************************************************************************************
   * smallest eval of K
   ***************************************************************************************/
  lambda[0] = -1.;
  
}  /* end of extreme_eval */

/***************************************************************************************
 *
 ***************************************************************************************/

void phi_force ( double * const f, double * const x, double ** const aux ) {
  
  /* f = K x */
  g_op( f, x );

  /* aux[0] = tanh(Kx) */
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    aux[0][i] = tanh( f[i] );
  }

  /* aux[1] = K aux[0] */
  g_op( aux[1], aux[0] );

  /* f = -f + aux[1] = - K x + K tanh ( Kx )*/
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    f[i] = -f[i] + aux[1][i] - hext[i];
  }

}  /* end of phi_force */


/***************************************************************************************
 *
 ***************************************************************************************/

void phi_force_2 ( double * const f, double * const x ) {
  
  /* f = mass x */
  v_eq_v( f, x );
  v_ti_eq_re( f, mass );

  /* f <- f - mass  tanh( f )
   *   = mass x - mass tanh ( mass x )
   */
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    f[i] -= mass * tanh( f[i] );
  }

}  /* end of phi_force */

/***************************************************************************************
 * H =  m p^2/2 -log ( cosh( mp ) ) + x K x - log ( cosh ( K x ) ) + x hext
 ***************************************************************************************/
double phi_energy_2 (double * const x, double * const p, double ** const aux ) {

  /* aux[0] <- K x */
  g_op ( aux[0], x );

  /* res1 <- x K x / 2 */
  double res1 = 0., res2 = 0.;
  re_eq_v_dot_v ( &res1, x, aux[0]);
  res1 *= 0.5;

  re_eq_v_dot_v ( &res2, p, p );
  res2 *= 0.5 * mass;

  /* res1 <- res1 - log( cosh( aux[0] ) ) */
#ifdef HAVE_OPENMP
  omp_lock_t writelock;
  omp_init_lock(&writelock);
#pragma omp parallel
{
#endif
  double res_tmp = 0., res_tmp2 = 0.;
#pragma omp for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    res_tmp  -= log( cosh( aux[0][i] ) ) + hext[i] * x[i];
    res_tmp2 -= log ( cosh ( mass * p[i] ) );
  }
#ifdef HAVE_OPENMP
  omp_set_lock(&writelock);
#endif
  res1 += res_tmp;
  res2 += res_tmp2;
#ifdef HAVE_OPENMP
   omp_unset_lock(&writelock);
}  /* end of parallel region */
  omp_destroy_lock(&writelock);
#endif

  return ( res1 + res2 );
}  /* end of phi_energy_2 */

/***************************************************************************************
 *
 ***************************************************************************************/
double phi_abs ( double * const x ) {

  double res = 0.;

#ifdef HAVE_OPENMP
  omp_lock_t writelock;
  omp_init_lock(&writelock);
#pragma omp parallel default(shared)
{
#endif

  double res2 = 0.;
#pragma omp for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    res2 += fabs ( x[i] );
  }

  #ifdef HAVE_OPENMP
  omp_set_lock(&writelock);
#endif
  res += res2;
#ifdef HAVE_OPENMP
  omp_unset_lock(&writelock);
}  /* end of parallel region */
  omp_destroy_lock(&writelock);
#endif

  return( res );
}  /* end of phi_abs */

/***************************************************************************************
 *
 ***************************************************************************************/
double phi_sign ( double * const x ) {

  double res = 0.;

#ifdef HAVE_OPENMP
  omp_lock_t writelock;
  omp_init_lock(&writelock);
#pragma omp parallel default(shared)
{
#endif

  double res2 = 0.;
#pragma omp for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    res2 += (double)( ( x[i] > 0 ) - ( x[i] < 0 ) );
  }

  #ifdef HAVE_OPENMP
  omp_set_lock(&writelock);
#endif
  res += res2;
#ifdef HAVE_OPENMP
  omp_unset_lock(&writelock);
}  /* end of parallel region */
  omp_destroy_lock(&writelock);
#endif

  return( res );
}  /* end of phi_abs */

/***************************************************************************************
 *
 ***************************************************************************************/
double phi_square ( double * const x ) {

  double res = 0.;

#ifdef HAVE_OPENMP
  omp_lock_t writelock;
  omp_init_lock(&writelock);
#pragma omp parallel default(shared)
{
#endif

  double res2 = 0.;
#pragma omp for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    res2 += x[i] * x[i];
  }

  #ifdef HAVE_OPENMP
  omp_set_lock(&writelock);
#endif
  res += res2;
#ifdef HAVE_OPENMP
  omp_unset_lock(&writelock);
}  /* end of parallel region */
  omp_destroy_lock(&writelock);
#endif

  return( res );
}  /* end of phi_abs */
