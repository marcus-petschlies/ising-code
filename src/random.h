#ifndef _RANDOM_H
#define _RANDOM_H

#ifdef HAVE_OPENMP
#include <omp.h>
#endif
#include "table_init_d.h"
#include "ranlxd.h"
#include "global.h"

/***************************************************************************************
 *
 ***************************************************************************************/
static inline double drandom(void) {
  double d;
  ranlxd ( &d, 1 );
  return ( d );
}  /* end of drandom */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void rangauss( double * const d, long long int const N ) {
  
  double const TWO_MPI = 2. * M_PI;
  double x[2];

  for ( long long int i = 0; i < N; i++ ) {
    ranlxd ( x, 2 );
    d[i] = sqrt( -2. * log( x[0] ) ) * cos( TWO_MPI * x[1] );
  }
}  /* end of rangauss */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void rangauss2( double * const d, double const mean, double const sd, long long int const N ) {

  rangauss( d, N );

#pragma omp parallel for
  for ( long long int i = 0; i < N; i++ ) {
    d[i] = sd * d[i] + mean;
  }
}  /* end of rangauss2 */


/***************************************************************************************
 *
 ***************************************************************************************/
static inline void ranbinary ( double * const d, long long int const N ) {
   
  double t;

  for ( long long int i = 0; i < N; i++ ) {
    ranlxd ( &t, 1 );
    d[i] = 2. * ( t>=0.5 ) - 1.;
  }

}  /* end of ranbinary */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void ranbinary2 ( double * const d, double const val0, double const val1, long long int const N ) {
   
  ranlxd ( d, N );

#pragma omp parallel for
  for ( long long int i = 0; i < N; i++ ) {
    int const t = ( d[i] >= 0.5 );
    d[i] = val0 * t + val1 * ( 1 - t );
  }

}  /* end of ranbinary2 */

/***************************************************************************************/
/***************************************************************************************/

/***************************************************************************************
 * sample from potential energy
 ***************************************************************************************/
static inline void ranpot (double * const d, long long int const N ) {

  double * x = init_1level_dtable ( N );
  double * u = init_1level_dtable ( N );
  if ( x == NULL || u == NULL ) {
    fprintf ( stderr, "[ranpot] Error from init_1level_table %s %d\n", __FILE__, __LINE__ );
    exit(1);
  }

  ranbinary2 ( u, 0, 1, N );
  rangauss2( d,  1., 1./sqrt(mass), N );
  rangauss2( x, -1., 1./sqrt(mass), N );
#pragma omp parallel for
  for ( long long int i = 0; i < N; i++ ) {
    d[i] = d[i] * u[i] + x[i] * ( 1 - u[i] );
  }

  fini_1level_dtable ( &x );
  fini_1level_dtable ( &u );
}

/***************************************************************************************/
/***************************************************************************************/
#endif
