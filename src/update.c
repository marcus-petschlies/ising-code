#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>

#include "table_init_d.h"
#include "global.h"
#include "geometry.h"
#include "linalg.h"
#include "random.h"
#include "utils.h"
#include "update.h"

/***************************************************************************************
 *
 ***************************************************************************************/
void update ( double * const x , double const t, double ** const aux ) {

  double * const x_old = x;

  double * const p_old = aux[0];
  double * const p_new = aux[1];
  double * const x_new = aux[2];

  /* suggest momentum */
  rangauss( p_old, VOLUME );

  run_eom ( x_new, p_new, x_old, p_old, t, &(aux[3]) );

  double const H_old = phi_energy ( x_old, p_old, &(aux[3]) );
  double const H_new = phi_energy ( x_new, p_new, &(aux[3]) );
  double const dH    =  H_new - H_old;
  if ( verbose > 1 ) fprintf ( stdout, "# [update] H_old %25.16e H_new %25.16e dH %16.7e\n", H_old, H_new, dH );

  if( drandom() < exp( -dH ) ) {
    v_eq_v ( x, x_new );
    if ( verbose > 1 ) fprintf ( stdout, "# [update] accept dH %25.16e\n", dH );
  } else {
    if ( verbose > 1 ) fprintf ( stdout, "# [update] refect dH %25.16e\n", dH );
  }
}  /* end of update */

/***************************************************************************************
 *
 ***************************************************************************************/
void update_hmc (double * const s, double const tau, unsigned int const nmd, double ** const aux )
{

  double const dtau  = tau / nmd;
  double const dtauh = dtau * 0.5;

  double * x_old = aux[0];
  double * x_new = aux[1];
  double * p_old = aux[2];
  double * p_new = aux[3];

  rangauss ( p_old, VOLUME );

  v_eq_v ( x_old, s );

  /* old energy */
  double const H_old = phi_energy ( x_old, p_old, &(aux[4]) );

  /* phi <- phi + epsilon/2 */
  v_pl_eq_v_ti_re ( x_old, p_old, dtauh );

  /* nmd-1 leapfrog steps */
  for ( unsigned int imd = 1; imd < nmd; imd++ ) {

    /* phi_force = -dH / dphi */
    phi_force ( aux[4], x_old , &(aux[5]) );

    /* p_new = p_old + dtau x phi_force */
    v_eq_v_pl_v_ti_re ( p_new, p_old, aux[4], dtau );

    /* x_new = x_old + dtau x p_new */
    v_eq_v_pl_v_ti_re ( x_new, x_old, p_new, dtau );

    /* swap x_old and x_new */
    double * swap = x_old;
    x_old = x_new;
    x_new = swap;

    /* swap p_old and p_new */
    swap  = p_old;
    p_old = p_new;
    p_new = swap;
  }  /* end of loop on nmd */

  /* final half leapfrog step */
  phi_force ( aux[4], x_old, &(aux[5]) );

  /* p_new <- p_old +  dtau x phi_force */
  v_eq_v_pl_v_ti_re ( p_new, p_old, aux[4], dtau );

  /* x_new <- x_old + dtau/2 x p_new  */
  v_eq_v_pl_v_ti_re ( x_new, x_old, p_new, dtauh );

  /* new energy */
  double const H_new = phi_energy ( x_new, p_new, &(aux[4]) );

  /* energy difference */
  double const dH = H_new - H_old;
  /* accept / reject step */
  if ( drandom() < exp( -dH ) ) {
    if ( verbose > 1 ) fprintf( stdout, "# [update_hmc] accept dH   %e exp(-dH) %e\n", dH , exp(-dH));
    v_eq_v ( s, x_new );
    accepted++;
  } else {
    if ( verbose > 1 ) fprintf( stdout, "# [update_hmc] reject dH   %e exp(-dH) %e\n", dH , exp(-dH));
  }
}  /* end of update_hmc */

/***************************************************************************************
 *
 ***************************************************************************************/
void update_constrained ( double * const x , double const tau, unsigned const nmd, double ** const aux ) {

  double * const x_old = aux[0];
  double * const p_old = aux[1];
  double * const p_new = aux[2];
  double * const x_new = aux[3];

  /* x_old <- x  */
  v_eq_v ( x_old, x );

  fprintf ( stdout, "\n\n" );
  for ( long long int i = 0; i < VOLUME; i++ ) {
    fprintf ( stdout, "x_old %8lld %25.16e\n", i, x_old[i] );
  }

  /* energy before rotation */
  double H_old;
  apply_J ( x_new, x_old );
  re_eq_v_dot_v ( &H_old, x_old, x_new );
 
  /* suggest momentum */
  ranbinary( p_old, VOLUME );

  /* for ( long long int i = 0; i < VOLUME; i++ ) {
    fprintf ( stdout, "p %8lld %25.16e\n", i, p_old[i] );
  } */


  /* rotate x */
  op_mat ( x_new, apply_J, x_old, tau, +1, 0, &(aux[4]) );

  /* rotate p */
  op_mat ( p_new, apply_J, p_old, tau, +1, 1, &(aux[4]) );

  /* add up */
  v_pl_eq_v ( x_new, p_new );

  /* project */
  v_eq_sign_v ( x_new, x_new );

  /* energy after rotation */
  double H_new;
  apply_J ( x_old, x_new );
  re_eq_v_dot_v ( &H_new, x_new, x_old );

  double const dH =  H_new - H_old;
  if ( verbose > 1 ) fprintf ( stdout, "# [update_constrained] H_old %25.16e H_new %25.16e dH %16.7e\n", H_old, H_new, dH );

  v_eq_v ( x, x_new );

  for ( long long int i = 0; i < VOLUME; i++ ) {
    fprintf ( stdout, "x_new %8lld %25.16e\n", i, x[i] );
  }

}  /* end of update_constrained */


/***************************************************************************************
 *
 ***************************************************************************************/
void wolff_step_periodic ( int * const s, long long int * const stack, double const padd ) {

  long long int i;
  long long int sp;
  long long int current, nn;

  i = (long long int) ( VOLUME * drandom() );

  stack[0] = i;
  sp = 1;
  int const oldspin =  s[i];
  int const newspin = -s[i];

  s[i] = newspin;

  while (sp) {

    current = stack[--sp];

    /* check the neighbours in all dimensions, up and dn */
    for ( unsigned int idim = 0; idim < dim; idim++ ) {

      nn = id_up[current][idim];
      if(s[nn] == oldspin) {
        if( drandom() < padd ) {
          stack[sp++] = nn;
          s[nn] = newspin;
        }
      }

      nn = id_dn[current][idim];
      if(s[nn] == oldspin) {
        if( drandom() < padd ) {
          stack[sp++] = nn;
          s[nn] = newspin;
        }
      }

    }  /* end of loop on dimensions */

  }  /* end of while sp */

}  /* end of wolff_step_periodic */


/***************************************************************************************
 *
 ***************************************************************************************/
void wolff_step_helical ( int * const s, long long int * const stack, double padd ) {

  long long int i;
  long long int sp;
  long long int current, nn;

  i = (long long int) ( VOLUME * drandom() );


  stack[0] = i;
  sp = 1;
  int const oldspin =  s[i];
  int const newspin = -s[i];

  s[i] = newspin;

  while (sp) {

    current = stack[--sp];

    /* check the neighbours */
    for ( unsigned int idim = 0; idim < dim; idim++ ) {

      /* nearest neighbour up */
      if ( (nn = current + LLconv[idim]) >= VOLUME ) nn -= VOLUME;
      if(s[nn] == oldspin) {
        if( drandom() < padd ) {
          stack[sp++] = nn;
          s[nn] = newspin;
        }
      }

      /* nearest neighbour down */
      if ( (nn = current - LLconv[idim]) < 0 ) nn += VOLUME;
      if(s[nn] == oldspin) {
        if( drandom() < padd ) {
          stack[sp++] = nn;
          s[nn] = newspin;
        }
      }

    }  /* end of loop on dimensions */

  }  /* end of while sp */

}  /* end of wolff_step_helical */

/***************************************************************************************/
/***************************************************************************************/

/***************************************************************************************
 *
 ***************************************************************************************/
void update_hmc_2 (double * const s, double const tau, unsigned int const nmd, double ** const aux )
{

  double const dtau  = tau / nmd;
  double const dtauh = dtau * 0.5;

  double * x_old = aux[0];
  double * x_new = aux[1];
  double * p_old = aux[2];
  double * p_new = aux[3];

  /* p_old <- sampled from potential energy distribution */
  ranpot ( p_old, VOLUME );

  /* x_old <- input vector */
  v_eq_v ( x_old, s );

  /* old energy */
  double const H_old = phi_energy_2 ( x_old, p_old, &(aux[4]) );

  /* 1st half-step
   * phi <- phi + epsilon/2 
   */
  /* aux[4] <- dH / dp */
  phi_force_2 ( aux[4], p_old );

  /* x_old <- x_old + aux[4] epsilon */
  v_pl_eq_v_ti_re ( x_old, aux[4], dtauh );

  /* nmd-1 full leapfrog steps */
  for ( unsigned int imd = 1; imd < nmd; imd++ ) {

    /* phi_force = -dH / dx */
    phi_force ( aux[4], x_old , &(aux[5]) );

    /* p_new <- p_old + dtau x phi_force */
    v_eq_v_pl_v_ti_re ( p_new, p_old, aux[4], dtau );

    /* aux[4] <- dH / dp from p_new */
    phi_force_2 ( aux[4], p_new );

    /* x_new <- x_old + dtau x aux[4] */
    v_eq_v_pl_v_ti_re ( x_new, x_old, aux[4], dtau );

    /* swap x_old and x_new */
    double * swap = x_old;
    x_old = x_new;
    x_new = swap;

    /* swap p_old and p_new */
    swap  = p_old;
    p_old = p_new;
    p_new = swap;
  }  /* end of loop on nmd */

  /* final half leapfrog step */
  phi_force ( aux[4], x_old, &(aux[5]) );

  /* p_new <- p_old +  dtau x phi_force */
  v_eq_v_pl_v_ti_re ( p_new, p_old, aux[4], dtau );

  /*  */
  phi_force_2 ( aux[4], p_new );

  /* x_new <- x_old + dtau/2 x p_new  */
  v_eq_v_pl_v_ti_re ( x_new, x_old, aux[4], dtauh );

  /* new energy */
  double const H_new = phi_energy_2 ( x_new, p_new, &(aux[4]) );

  /* energy difference */
  double const dH = H_new - H_old;
  /* accept / reject step */
  if ( drandom() < exp( -dH ) ) {
    if ( verbose > 1 ) fprintf( stdout, "# [update_hmc_2] accept dH   %e exp(-dH) %e\n", dH , exp(-dH));
    v_eq_v ( s, x_new );
    accepted++;
  } else {
    if ( verbose > 1 ) fprintf( stdout, "# [update_hmc_2] reject dH   %e exp(-dH) %e\n", dH , exp(-dH));
  }
}  /* end of update_hmc */
