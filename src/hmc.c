#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#include "ranlxd.h"

#ifdef HAVE_OPENMP
#include <omp.h>
#endif

#define MAIN_PROGRAM
#include "global.h"
#include "geometry.h"
#include "linalg.h"
#include "table_init_d.h"
#include "random.h"
#include "utils.h"
#include "update.h"

/***************************************************************************************
 *
 ***************************************************************************************/


int main(int argc, char **argv) {

  int c;
  int seed = 0;
  char LL_str[200] = "NA";
  unsigned int Niter = 0, every = 1;
  double tau = 0;
  unsigned int nmd = 0;
  double hext_value = 0.;

  init();

  while ((c = getopt(argc, argv, "h:N:J:s:L:d:v:m:T:M:e:H:")) != -1) {
    switch (c) {
      case 'd':
        dim = atoi(optarg);
        fprintf(stdout, "# [hmc] dimension set to %u\n", dim );
        break;
      case 'J':
        J = atof(optarg);
        fprintf(stdout, "# [hmc] interaction energy J set to %f\n", J);
        break;
      case 'm':
        mass = atof(optarg);
        fprintf(stdout, "# [hmc] mass set to %f\n", mass );
        break;
      case 'L':
        strcpy( LL_str, optarg);
        fprintf(stdout, "# [hmc] lattice size string set to %s\n", LL_str);
        break;
      case 's':
        seed = atoi(optarg);
        fprintf(stdout, "# [hmc] seed = %i\n", seed );
        break;
      case 'T':
        tau = atof ( optarg );
        fprintf(stdout, "# [hmc] tau = %f\n", tau );
        break;
      case 'M':
        nmd = atoi ( optarg );
        fprintf ( stdout, "# [hmc] nmd = %d\n", nmd );
        break;
      case 'N':
        Niter = atoi(optarg);
        fprintf(stdout, "# [hmc] Niter = %u\n", Niter );
        break;
      case 'e':
        every = atoi ( optarg );
        fprintf(stdout, "# [hmc] every = %u\n", every );
        break;
      case 'H':
        hext_value = atof ( optarg );
        fprintf(stdout, "# [hmc] hext_value = %f\n", hext_value );
        break;
      case 'v':
        verbose = atoi(optarg);
        fprintf(stdout, "# [hmc] verbosity level = %d\n", verbose );
        break;
      default:
        exit(1);
    }
  }

  /******************************************************
   * report git version
   ******************************************************/
  fprintf(stdout, "# [hmc] git version = %s\n", gitversion);

  /******************************************************
   * initialize random number generator
   ******************************************************/
  rlxd_init ( 2, seed);

  /******************************************************
   * check dim
   ******************************************************/
  if ( dim == 0 ) {
    fprintf ( stderr, "[hmc] Error, need dim\n" );
    exit(1);
  }

  /******************************************************
   * check omp setup
   ******************************************************/
#ifdef HAVE_OPENMP
  omp_set_num_threads ( num_threads );
#pragma omp parallel
{
  fprintf ( stdout, "# [hmc] thread no. %2d reports number of threads %d\n", omp_get_thread_num(), omp_get_num_threads () );
}
#endif


  /******************************************************
   * check lattice size string
   ******************************************************/
  if ( strcmp( LL_str, "NA" ) == 0 ) {
    fprintf ( stderr, "[hmc] Error, LL string\n" );
    exit(2);
  }

  /******************************************************
   * set lattice dimensions
   ******************************************************/
  LL = (unsigned int *)calloc ( dim, sizeof(unsigned int ) );
  char * ptr = strtok ( LL_str, "," );
  if ( ptr == NULL ) exit(3);
  LL[0] = atoi ( ptr );
  for ( unsigned int i = 1; i < dim; i++ ) {
    ptr = strtok ( NULL, "," );
    if ( ptr == NULL ) exit(3);
    LL[i] = atoi ( ptr );
  }

  for ( unsigned int i = 0; i < dim; i++ ) {
    fprintf ( stdout, "# [hmc] LL[%u] = %4u\n", i, LL[i] );
  }

  /******************************************************
   * set VOLUME
   ******************************************************/
  VOLUME = 1;
  for ( unsigned int i = 0; i < dim; i++ ) {
    VOLUME *= LL[i];
  }
  fprintf ( stdout, "# [hmc] VOLUME = %lld\n", VOLUME );

  /******************************************************
   * external magnetic field
   ******************************************************/
  hext = init_1level_dtable ( VOLUME );
  v_eq_re ( hext, hext_value );


  /******************************************************
   * init nearest neighbour indices
   ******************************************************/
  init_nn_fields ();

  /******************************************************
   * allocate spin field
   ******************************************************/

  double ** aux = init_2level_dtable ( num_aux_field, VOLUME );
  double ** s   = init_2level_dtable ( 2, VOLUME );

  if( s == NULL || aux == NULL ) {
    fprintf(stderr, "[hmc] Error from init_2level_dtable\n");
    exit(4);
  }

  /* output */
  FILE * ofs = NULL;
  char filename[200];
  sprintf ( filename, "hmc.log" );
  ofs = fopen ( filename, "w" );
  if ( ofs == NULL ) {
    fprintf(stderr, "[hmc] Error from fopen\n");
    exit(5);
  }
  fprintf( ofs, "# J      = %f\n", J );
  fprintf( ofs, "# mass   = %f\n", mass );
  fprintf( ofs, "# hext   = %f\n", hext_value );
  fprintf( ofs, "# dim    = %u\n", dim );
  for( unsigned int i = 0; i < dim; i++ ) {
    fprintf( ofs, "# L %u = %u\n", i, LL[i] );
  }
  fprintf( ofs, "# seed   = %d\n", seed );
  fprintf( ofs, "# tau    = %f\n", tau );
  fprintf( ofs, "# nmd    = %u\n", nmd );
  fprintf( ofs, "# Niter  = %u\n", Niter );
  fprintf( ofs, "# every  = %u\n", every );

  unsigned int * coords = (unsigned int *) malloc ( dim * sizeof(unsigned int) );
  if ( coords == NULL ) {
    fprintf ( stderr, "[hmc] Error from malloc\n");
    exit(2);
  }

  /******************************************************
   * initialize standard-normal s field 
   ******************************************************/
  rangauss( s[0], VOLUME );

  double ratime = _GET_TIME;

  accepted = 0;
  unsigned int nmeas = 0;
  /******************************************************
   * iterate
   ******************************************************/
  for ( unsigned int iter = 0; iter < Niter; iter++ ) {

    update_hmc ( s[0], tau, nmd, aux );
 
    if ( ( iter + 1 ) % every == 0 ) {
      double const magn = phi_magnetization ( s[0] );
      double const phia = phi_abs ( s[0] );
      double const phis = phi_sign ( s[0] );
      double const phi2 = phi_square ( s[0] );
      fprintf ( ofs, "%6d %25.16e %25.16e %25.16e %25.16e\n", nmeas, magn/VOLUME, phia/VOLUME, phis/VOLUME, phi2/VOLUME );
      nmeas++;
    }

  }  /* end of loop on iter */

  double retime = _GET_TIME;
  fprintf ( ofs, "# acceptance = %e\n", (double)accepted / (double)Niter );
  fprintf ( ofs, "# time       = %e\n", retime-ratime );

  fclose ( ofs );


  /******************************************************
   * deallocate
   ******************************************************/
  fini_2level_dtable ( &s );
  fini_2level_dtable ( &aux );
  fini_1level_dtable ( &hext );
  free ( coords );

  fini();
  return(0);
}
