#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')
plot=$MyName.gpl
LOG=$MyName.log
echo "# [$MyName] `date`"  > $LOG

doPlot="yes"
doTex="yes"

fermi="\\,\\mathrm{fm}"
gev="\\,\\mathrm{GeV}"
mev="\\,\\mathrm{MeV}"

cat << EOF > $plot
# [$MyName] `date`
set terminal epslatex solid color

f(x,m) = 0.5*x**2*m - log(cosh(m*x))


set xlabel ' $ |\\phi_i| $ '
xmin=0
xmax=5
set xrange [xmin:xmax]
# set xtics ( 4, 8, 16, 32, 64, 128, 256, 512)

# set key at 1500, 340
set key bottom right

# set ytics (1, 2, 4, 6, 8)
set bar 0
set pointsize 1.0
#set logscale x
#set logscale y

set output "poten_single.tex"
set ylabel ' $ V\\left( |\\phi_i| \right)  $ '
set format y "%3.1f"
ymin = -5.0
ymax =  3
set yrange [ymin:ymax]

set title ' $ V\\left( |\\phi_i| \right) = \\frac{1}{2}\\,m\,|\\phi_i|^2 - \\log(\\cosh(m|\\phi_i|)) $ '
plot \\
  f(x,0.0) w lines lc rgb "black" title ' $ m=0 $', \\
  f(x,0.8) w lines lc rgb "red" title ' $ m=0.8 $', \\
  f(x,0.4) w lines lc rgb "dark-red" title ' $ m=0.4 $', \\
  f(x,1.0) w lines lc rgb "blue" title ' $ m=1.0 $', \\
  f(x,4.0) w lines lc rgb "dark-green" title ' $ m=4.0 $', \\
  f(x,10.0) w lines lc rgb "dark-yellow" title ' $ m=10.0 $'

#set output "poten_single_min.tex"
#set title ' $ |\\phi_i|^* = \\mathrm{arg~min} \\,V\\left( |\\phi_i| \\right) $ '
#set ylabel ' $ |\\phi_i|^* $ '
#ymin = -0.5
#ymax = 10.
#set yrange [ymin:ymax]
#xmin=0
#xmax=4
#set xrange [xmin:xmax]
#set key right top
#plot \\
#  "../R/poten.min.tab" u 1:2 w lines lc "dark-blue" title 'position of miminum'


EOF

echo "reset" >> $plot

if [ "X$doPlot" == "Xyes" ]; then
  gnuplot $plot
  es=$?
  if [ $es -ne 0 ]; then
    echo "Error from gnuplot, status was $es"
    exit 1
  fi

  outputFileList=($(awk '!/^#/ && /set\ output/ {gsub(/"/,"",$NF);print $NF}' $plot))
  echo "# [] output file list = ${outputFileList[*]}"

  outputFileListTex=(${outputFileList[*]})
  outputFileListEps=($(echo ${outputFileList[*]} | awk '{gsub(/\.tex/,".eps",$0); print}'))

  if [ "X$syncPlots" == "Xyes" ]; then
    cp -av ${outputFileListTex[*]} ${outputFileListEps[*]} $syncPath/
  fi

  if [ "X$doTex" == "Xyes" ]; then
    for outputFile in ${outputFileList[*]}; do
      filePrefix=$(echo $outputFile | awk '{sub(/\.tex$/,"",$0);print}')
      echo "# [] $outputFile --- $filePrefix"
      texFile=${filePrefix}_wrapper.tex
      epsFile=${filePrefix}_wrapper.eps
      dviFile=${filePrefix}_wrapper.dvi
      pdfFile=${filePrefix}_wrapper.pdf
      pdfFileCropped=${filePrefix}_wrapper_cropped.pdf
cat << EOF
      # [] $texFile
      # [] $epsFile
      # [] $dviFile
      # [] $pdfFile
EOF
      rm -f ${filePrefix}_wrapper.{log,aux,out}
cat << EOF > $texFile
\documentclass[11pt]{article}
\usepackage{graphicx,color}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amscd}
\usepackage{bbold}
\setlength{\textwidth}{16cm}
\begin{document}
\pagestyle{empty}
{\small
\input{$outputFile}
}
\end{document}
EOF

      latex $texFile && latex $texFile
      dvips -E -o $epsFile $dviFile
      epstopdf $epsFile
      rm -f ${filePrefix}_wrapper.{log,aux,dvi}
      pdfcrop $pdfFile && mv ${filePrefix}_wrapper-crop.pdf ${filePrefix}.pdf
      rm ${filePrefix}_wrapper.{tex,eps,pdf} ${filePrefix}.{tex,eps}
    done # >> $LOG 2>&1
  fi  # end of if doTex
fi    # end of if doPlot

