#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#include "ranlxd.h"

#ifdef HAVE_OPENMP
#include <omp.h>
#endif

#define MAIN_PROGRAM
#include "global.h"
#include "table_init_d.h"
#include "utils.h"

/***************************************************************************************
 *
 ***************************************************************************************/


int main(int argc, char **argv) {

  int c;
  int seed = 0;
  char LL_str[200] = "NA";
  unsigned int Niter = 0, every = 1;
  double tau = 0;

  init();

  while ((c = getopt(argc, argv, "h:N:J:s:L:d:v:m:t:e:")) != -1) {
    switch (c) {
      case 'd':
        dim = atoi(optarg);
        fprintf(stdout, "# [hybrid_constrained] dimension set to %u\n", dim );
        break;
      case 'J':
        J = atof(optarg);
        fprintf(stdout, "# [hybrid_constrained] interaction energy J set to %f\n", J);
        break;
      case 'm':
        mass = atof(optarg);
        fprintf(stdout, "# [hybrid_constrained] mass set to %f\n", mass );
        break;
      case 'L':
        strcpy( LL_str, optarg);
        fprintf(stdout, "# [hybrid_constrained] lattice size string set to %s\n", LL_str);
        break;
      case 's':
        seed = atoi(optarg);
        fprintf(stdout, "# [hybrid_constrained] seed = %i\n", seed );
        break;
      case 't':
        tau = atof ( optarg );
        fprintf(stdout, "# [hybrid_constrained] tau = %f\n", tau );
        break;
      case 'N':
        Niter = atoi(optarg);
        fprintf(stdout, "# [hybrid_constrained] Niter = %u\n", Niter );
        break;
      case 'e':
        every = atoi ( optarg );
        fprintf(stdout, "# [hybrid_constrained] every = %u\n", every );
        break;
      case 'v':
        verbose = atoi(optarg);
        fprintf(stdout, "# [hybrid_constrained] verbosity level = %d\n", verbose );
        break;
      default:
        exit(1);
    }
  }

  /******************************************************
   * report git version
   ******************************************************/
  fprintf(stdout, "# [hybrid_constrained] git version = %s\n", gitversion);

  /******************************************************
   * initialize random number generator
   ******************************************************/
  rlxd_init ( 2, seed);

  /******************************************************
   * check dim
   ******************************************************/
  if ( dim == 0 ) {
    fprintf ( stderr, "[hybrid_constrained] Error, need dim\n" );
    exit(1);
  }

  /******************************************************
   * check omp setup
   ******************************************************/
#ifdef HAVE_OPENMP
  omp_set_num_threads ( num_threads );
#pragma omp parallel
{
  fprintf ( stdout, "# [hybrid_constrained] thread no. %2d reports number of threads %d\n", omp_get_thread_num(), omp_get_num_threads () );
}
#endif


  /******************************************************
   * check lattice size string
   ******************************************************/
  if ( strcmp( LL_str, "NA" ) == 0 ) {
    fprintf ( stderr, "[hybrid_constrained] Error, LL string\n" );
    exit(2);
  }

  /******************************************************
   * set lattice dimensions
   ******************************************************/
  LL = (unsigned int *)calloc ( dim, sizeof(unsigned int ) );
  char * ptr = strtok ( LL_str, "," );
  if ( ptr == NULL ) exit(3);
  LL[0] = atoi ( ptr );
  for ( unsigned int i = 1; i < dim; i++ ) {
    ptr = strtok ( NULL, "," );
    if ( ptr == NULL ) exit(3);
    LL[i] = atoi ( ptr );
  }

  for ( unsigned int i = 0; i < dim; i++ ) {
    fprintf ( stdout, "# [hybrid_constrained] LL[%u] = %4u\n", i, LL[i] );
  }

  /******************************************************
   * set VOLUME
   ******************************************************/
  VOLUME = 1;
  for ( unsigned int i = 0; i < dim; i++ ) {
    VOLUME *= LL[i];
  }
  fprintf ( stdout, "# [hybrid_constrained] VOLUME = %lld\n", VOLUME );


  /******************************************************
   * init nearest neighbour indices
   ******************************************************/
  init_nn_fields ();

  /******************************************************
   * allocate spin field
   ******************************************************/

  double ** aux = init_2level_dtable ( num_aux_field, VOLUME );
  double ** s   = init_2level_dtable ( 2, VOLUME );

  if( s == NULL || aux == NULL ) {
    fprintf(stderr, "[hybrid_constrained] Error from init_2level_dtable\n");
    exit(4);
  }

  /* output */
  FILE * ofs = NULL;
  char filename[200];
  sprintf ( filename, "hybrid_constrained.log" );
  ofs = fopen ( filename, "w" );
  if ( ofs == NULL ) {
    fprintf(stderr, "[hybrid_constrained] Error from fopen\n");
    exit(5);
  }
  fprintf( ofs, "# J      = %f\n", J );
  fprintf( ofs, "# dim    = %u\n", dim );
  for( unsigned int i = 0; i < dim; i++ ) {
    fprintf( ofs, "# L %u = %u\n", i, LL[i] );
  }
  fprintf( ofs, "# seed   = %d\n", seed );
  fprintf( ofs, "# tau    = %f\n", tau );
  fprintf( ofs, "# Niter  = %u\n", Niter );
  fprintf( ofs, "# every  = %u\n", every );

  unsigned int * coords = (unsigned int *) malloc ( dim * sizeof(unsigned int) );
  if ( coords == NULL ) {
    fprintf ( stderr, "[hybrid_constrained] Error from malloc\n");
    exit(2);
  }

  /******************************************************
   * initialize standard-normal s field 
   ******************************************************/
  ranbinary( s[0], VOLUME );

  double ratime = _GET_TIME;

  unsigned int nmeas = 0;
  /******************************************************
   * iterate
   ******************************************************/
  for ( unsigned int iter = 0; iter < Niter; iter++ ) {
    update_constrained ( s[0], tau, 0, aux );
#if 0 
    if ( ( iter + 1 ) % every == 0 ) {
      double const magn = phi_magnetization ( s[0], aux );
      double const w    = phi_weight ( s[0], aux );
      fprintf ( ofs, "%6d %25.16e %25.16e\n", nmeas, magn, w );
      nmeas++;
    }
#endif
  }  /* end of loop on iter */

  double retime = _GET_TIME;
  fprintf ( ofs, "# time / s = %e\n", retime-ratime );


  /******************************************************
   * deallocate
   ******************************************************/
  fini_2level_dtable ( &s );
  fini_2level_dtable ( &aux );
  free ( coords );

  fini();
  return(0);
}
