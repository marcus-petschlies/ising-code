#ifndef _GLOBAL_H
#define _GLOBAL_H

#if defined MAIN_PROGRAM
#  define EXTERN
#else
#  define EXTERN extern
#endif 
#define MAX_TRIG_ORDER 20
#define MAX_GEOM_ORDER 100
#define _GET_TIME ((double)clock() / CLOCKS_PER_SEC)

typedef void apply_op_type ( double * const , double * const );

EXTERN int verbose;
EXTERN unsigned int dim;
EXTERN unsigned int *LL;
EXTERN long long int *LLconv;
EXTERN long long int VOLUME;
EXTERN long long int ** id_up;
EXTERN long long int ** id_dn;

EXTERN double J;
EXTERN double mass;
EXTERN double * hext;

EXTERN apply_op_type * g_op;
EXTERN int num_threads;

EXTERN int num_aux_field;

EXTERN unsigned int accepted;

extern const char * gitversion;

#endif
