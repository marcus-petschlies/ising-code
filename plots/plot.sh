#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')
plot=$MyName.gpl
LOG=$MyName.log
echo "# [$MyName] `date`"  > $LOG

doPlot="yes"
doTex="yes"

fermi="\\,\\mathrm{fm}"
gev="\\,\\mathrm{GeV}"
mev="\\,\\mathrm{MeV}"

cat << EOF > $plot
# [$MyName] `date`
set terminal epslatex solid color

f(x) = exp(a)*x**z
z               = 0.451346  #       +/- 0.00533      (1.181%)
a               = -0.568635 #       +/- 0.01855      (3.261%)


set xlabel ' $ L $ '

xmin=3
xmax=600
set xrange [xmin:xmax]
set xtics ( 4, 8, 16, 32, 64, 128, 256, 512)

# set key at 1500, 340
set key bottom right

set ytics (1, 2, 4, 6, 8)
set bar 0
set logscale x
set logscale y

set output "wolff_tauint.tex"
set ylabel ' $ \\bar{\\tau}_{\\mathrm{int},W}  $ '
set pointsize 1.0
# set format y "%3.0f"
ymin = 1
ymax = 8
set yrange [ymin:ymax]
set bar 0

set label ' $ z =  0.451\\,(5) $ ' at 8, 6
set label ' $ a = -0.57 \\,(2) $ ' at 8, 5

plot \\
  "m.uwerr" u (\$1):(\$5):(\$6) w ye pt  3 lc rgb "red" title 'Wolff cluster algorithm, $ T_c / J = 2 / \\log\\left( 1 + \\sqrt{2} \\right) $ ',\\
  f(x) w lines lc rgb "blue" title 'fit with $ f(L) = a\\,L^z $, $ \\chi^2/\\mathrm{dof} = 0.922855 $ '

EOF

echo "reset" >> $plot

if [ "X$doPlot" == "Xyes" ]; then
  gnuplot $plot
  es=$?
  if [ $es -ne 0 ]; then
    echo "Error from gnuplot, status was $es"
    exit 1
  fi

  outputFileList=($(awk '!/^#/ && /set\ output/ {gsub(/"/,"",$NF);print $NF}' $plot))
  echo "# [] output file list = ${outputFileList[*]}"

  outputFileListTex=(${outputFileList[*]})
  outputFileListEps=($(echo ${outputFileList[*]} | awk '{gsub(/\.tex/,".eps",$0); print}'))

  if [ "X$syncPlots" == "Xyes" ]; then
    cp -av ${outputFileListTex[*]} ${outputFileListEps[*]} $syncPath/
  fi

  if [ "X$doTex" == "Xyes" ]; then
    for outputFile in ${outputFileList[*]}; do
      filePrefix=$(echo $outputFile | awk '{sub(/\.tex$/,"",$0);print}')
      echo "# [] $outputFile --- $filePrefix"
      texFile=${filePrefix}_wrapper.tex
      epsFile=${filePrefix}_wrapper.eps
      dviFile=${filePrefix}_wrapper.dvi
      pdfFile=${filePrefix}_wrapper.pdf
      pdfFileCropped=${filePrefix}_wrapper_cropped.pdf
cat << EOF
      # [] $texFile
      # [] $epsFile
      # [] $dviFile
      # [] $pdfFile
EOF
      rm -f ${filePrefix}_wrapper.{log,aux,out}
cat << EOF > $texFile
\documentclass[11pt]{article}
\usepackage{graphicx,color}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amscd}
\usepackage{bbold}
\setlength{\textwidth}{16cm}
\begin{document}
\pagestyle{empty}
{\small
\input{$outputFile}
}
\end{document}
EOF

      latex $texFile && latex $texFile
      dvips -E -o $epsFile $dviFile
      epstopdf $epsFile
      rm -f ${filePrefix}_wrapper.{log,aux,dvi}
      pdfcrop $pdfFile && mv ${filePrefix}_wrapper-crop.pdf ${filePrefix}.pdf
      rm ${filePrefix}_wrapper.{tex,eps,pdf} ${filePrefix}.{tex,eps}
    done # >> $LOG 2>&1
  fi  # end of if doTex
fi    # end of if doPlot

