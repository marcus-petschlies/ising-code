#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#include "ranlxd.h"

#define MAIN_PROGRAM
#include "global.h"
#include "utils.h"

/***************************************************************************************
 *
 ***************************************************************************************/


void metropolis_update ( int * const s ) {

  for ( long long int i = 0; i < VOLUME; i++ ) {

    long long int const x = (long long int) ( VOLUME * drandom() );

    int const oldspin =  s[x];
    int const newspin = -s[x];

    int staple = 0;

    for ( unsigned int k = 0; k < dim; k++ ) {
      staple += s[id_up[x][k]] + s[id_dn[x][k]];
    }

    double dH = 2. * oldspin * J * staple;

    double r = drandom();

    if ( r < exp(-dH) ) s[x] = newspin;

  }  /* end of loop on VOLUME */

}  /* end of metropolis_update */



int main(int argc, char **argv) {

  int c;
  unsigned int Niter = 0;
  int *s = NULL;
  int seed = 0;
  double heat = 0.;
  unsigned int every = 1;
  char LL_str[200] = "NA";

  init();

  while ((c = getopt(argc, argv, "h:N:J:s:e:L:d:v:")) != -1) {
    switch (c) {
      case 'd':
        dim = atoi(optarg);
        fprintf(stdout, "# [metropolis] dimension set to %u\n", dim );
        break;
      case 'J':
        J = atof(optarg);
        fprintf(stdout, "# [metropolis] interaction energy J set to %f\n", J);
        break;
      case 'h':
        heat = atof(optarg);
        fprintf(stdout, "# [metropolis] initial heat set o %f\n", heat);
        break;
      case 'N':
        Niter = atoi(optarg);
        fprintf(stdout, "# [metropolis] number of iterations set to %i\n", Niter);
        break;
      case 'L':
        strcpy( LL_str, optarg);
        fprintf(stdout, "# [metropolis] lattice size string set to %s\n", LL_str);
        break;
      case 's':
        seed = atoi(optarg);
        fprintf(stdout, "# [metropolis] seed = %i\n", seed );
        break;
      case 'e':
        every = atoi(optarg);
        fprintf(stdout, "# [metropolis] every = %u\n", every );
        break;
      case 'v':
        verbose = atoi(optarg);
        fprintf(stdout, "# [metropolis] verbosity level = %d\n", verbose );
        break;
      default:
        exit(1);
    }
  }

  /******************************************************
   * report git version
   ******************************************************/
  fprintf(stdout, "# [metropolis] git version = %s\n", gitversion);

  /******************************************************
   * initialize random number generator
   ******************************************************/
  rlxd_init ( 2, seed);

  /******************************************************
   * check dim
   ******************************************************/
  if ( dim == 0 ) {
    fprintf ( stderr, "[metropolis] Error, need dim\n" );
    exit(1);
  }

  /******************************************************
   * check lattice size string
   ******************************************************/
  if ( strcmp( LL_str, "NA" ) == 0 ) {
    fprintf ( stderr, "[metropolis] Error, LL string\n" );
    exit(2);
  }

  /******************************************************
   * set lattice dimensions
   ******************************************************/
  LL = (unsigned int *)calloc ( dim, sizeof(unsigned int ) );
  char * ptr = strtok ( LL_str, "," );
  if ( ptr == NULL ) exit(3);
  LL[0] = atoi ( ptr );
  for ( unsigned int i = 1; i < dim; i++ ) {
    ptr = strtok ( NULL, "," );
    if ( ptr == NULL ) exit(3);
    LL[i] = atoi ( ptr );
  }

  for ( unsigned int i = 0; i < dim; i++ ) {
    fprintf ( stdout, "# [metropolis] LL[%u] = %4u\n", i, LL[i] );
  }

  /******************************************************
   * set VOLUME
   ******************************************************/
  VOLUME = 1;
  for ( unsigned int i = 0; i < dim; i++ ) {
    VOLUME *= LL[i];
  }
  fprintf ( stdout, "# [metropolis] VOLUME = %lld\n", VOLUME );


  /******************************************************
   * init nearest neighbour indices
   ******************************************************/
  init_nn_fields ();

  /******************************************************
   * allocate spin field
   ******************************************************/
  s = ( int * )malloc( VOLUME * sizeof(int) );

  if( s == NULL ) {
    fprintf(stderr, "[metropolis] Error from malloc\n");
    exit(4);
  }

  /* initialize the lattice */
  for ( long long int i = 0; i < VOLUME; i++) {
    s[i] = 2*(int)( heat * drandom() + 1-heat > 0.5 ) - 1;
  }
  if ( dim == 2 && verbose > 0 ) print_s ( s );

  /* output file */
  FILE *ofs = NULL;
  char filename[100];
  sprintf ( filename, "metropolis.log" );
  ofs = fopen ( filename, "w" );

  fprintf( ofs, "# J      = %f\n", J );
  fprintf( ofs, "# dim    = %u\n", dim );
  for( unsigned int i = 0; i < dim; i++ ) {
    fprintf( ofs, "# L %u = %u\n", i, LL[i] );
  }
  fprintf( ofs, "# seed   = %d\n", seed );
  fprintf( ofs, "# heat   = %f\n", heat);
  fprintf( ofs, "# Niter  = %u\n", Niter );

  double const ratime = ((double)clock() / CLOCKS_PER_SEC);

  /* iterate with the Wolff cluster algorithm */
  for ( unsigned int iter = 0; iter < Niter; iter++)
  {
    metropolis_update ( s );

    /* print_s(s); */

    if ( ( iter + 1 ) % every == 0 ) {
      fprintf(ofs, "m %8u %25.16e\n", iter, magnetization(s) );
    }

  }
#if 0
#endif
  double const retime = ((double)clock() / CLOCKS_PER_SEC);
  fprintf ( ofs, "# time   = %e seconds\n", retime - ratime );

  fclose ( ofs );


  /******************************************************
   * deallocate
   ******************************************************/
  free(s);

  fini();
  return(0);
}
