#!/bin/bash
J=1.
BETA_C=$(echo "scale=16; 0.4406868 / $J" | bc -l)

NUM_BETA=100
BETA_MIN=$(echo "scale=16; 0.80 * $BETA_C" | bc -l)
BETA_MAX=$(echo "scale=16; 1.25 * $BETA_C" | bc -l)

NSKIP=100000
OUTPUT_FILE=wolff.stats

export R_LIBS=$HOME/lib/R

for((i=76; i<100; i++)); do

  BETA=$(echo "scale=16; $BETA_MIN + ($BETA_MAX - $BETA_MIN) * $i / $NUM_BETA" | bc -l)
  echo "# [] BETA = $BETA"

  DATA_FILE=out.$i

  if [ $i == 0 ]; then
    APPEND=FALSE
  else
    APPEND=TRUE
  fi

  APPEND=TRUE

  R CMD BATCH --args "-b $BETA -a $APPEND -N $NSKIP -o $OUTPUT_FILE -d $DATA_FILE" analyse.R

done




