#!/bin/bash
MyName=$(echo $0 | awk -F\/ '{sub(/\.sh$/,"",$NF);print $NF}')
plot=$MyName.gpl
LOG=$MyName.log
echo "# [$MyName] `date`"  > $LOG

doPlot="yes"
doTex="yes"

fermi="\\,\\mathrm{fm}"
gev="\\,\\mathrm{GeV}"
mev="\\,\\mathrm{MeV}"

cat << EOF > $plot
# [$MyName] `date`
set terminal epslatex solid color

xmin=-1
xmax=33
set xrange [xmin:xmax]

ymin = -0.01
ymax = 0.09
set yrange [ymin:ymax]

# set key at 1500, 340
set key bottom top

# set ytics (1, 2, 4, 6, 8)
# set logscale x
# set logscale y

set output "bootstrap_corr.tex"

set xlabel ' $ r $ '
set ylabel ' $ g(r) $ '

set pointsize 1.0
# set format y "%3.0f"
# set bar 0

#set label ' $ z =  0.451\\,(5) $ ' at 8, 6

plot \\
  "bootstrap_corr.tab" u (\$1):(\$5):(\$6) w filledcurves lc rgb "#444444"  title '68 \% confidence band', \\
  "T64_L64_q0_wm.corr" u (\$1):(\$2):(\$3) w ye lc rgb "red"  title 'data'

#  "bootstrap_corr.tab" u (\$1):(\$4) w lines lc rgb "red"  title 'median'

EOF

echo "reset" >> $plot

if [ "X$doPlot" == "Xyes" ]; then
  gnuplot $plot
  es=$?
  if [ $es -ne 0 ]; then
    echo "Error from gnuplot, status was $es"
    exit 1
  fi

  outputFileList=($(awk '!/^#/ && /set\ output/ {gsub(/"/,"",$NF);print $NF}' $plot))
  echo "# [] output file list = ${outputFileList[*]}"

  outputFileListTex=(${outputFileList[*]})
  outputFileListEps=($(echo ${outputFileList[*]} | awk '{gsub(/\.tex/,".eps",$0); print}'))

  if [ "X$syncPlots" == "Xyes" ]; then
    cp -av ${outputFileListTex[*]} ${outputFileListEps[*]} $syncPath/
  fi

  if [ "X$doTex" == "Xyes" ]; then
    for outputFile in ${outputFileList[*]}; do
      filePrefix=$(echo $outputFile | awk '{sub(/\.tex$/,"",$0);print}')
      echo "# [] $outputFile --- $filePrefix"
      texFile=${filePrefix}_wrapper.tex
      epsFile=${filePrefix}_wrapper.eps
      dviFile=${filePrefix}_wrapper.dvi
      pdfFile=${filePrefix}_wrapper.pdf
      pdfFileCropped=${filePrefix}_wrapper_cropped.pdf
cat << EOF
      # [] $texFile
      # [] $epsFile
      # [] $dviFile
      # [] $pdfFile
EOF
      rm -f ${filePrefix}_wrapper.{log,aux,out}
cat << EOF > $texFile
\documentclass[11pt]{article}
\usepackage{graphicx,color}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amscd}
\usepackage{bbold}
\setlength{\textwidth}{16cm}
\begin{document}
\pagestyle{empty}
{\small
\input{$outputFile}
}
\end{document}
EOF

      latex $texFile && latex $texFile
      dvips -E -o $epsFile $dviFile
      epstopdf $epsFile
      rm -f ${filePrefix}_wrapper.{log,aux,dvi}
      pdfcrop $pdfFile && mv ${filePrefix}_wrapper-crop.pdf ${filePrefix}.pdf
      rm ${filePrefix}_wrapper.{tex,eps,pdf} ${filePrefix}.{tex,eps}
    done # >> $LOG 2>&1
  fi  # end of if doTex
fi    # end of if doPlot

