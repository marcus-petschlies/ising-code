#ifndef _GEOMETRY_H
#define _GEOMETRY_H

#ifdef HAVE_OPENMP
#include <omp.h>
#endif
#include "global.h"

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void lexic2coords (unsigned int * const coords, long long int const x ) {

  coords[0] = x / LLconv[0];
  for ( unsigned int i = 1; i < dim; i++ ) {
    coords[i] = ( x % LLconv[i-1] ) / LLconv[i];
  }

}  /* end of lexic2coords */

/***************************************************************************************
 * periodic boundary conditions
 ***************************************************************************************/
static inline long long int coords2lexic ( unsigned int * const coords ) {

  long long int x = 0;
  for ( unsigned int k = 0; k < dim; k++ ) {
    x += ( coords[k] % LL[k] ) * LLconv[k];
  }
  return ( x );
}  /* end of coords2lexic */

/***************************************************************************************/
/***************************************************************************************/

#endif
