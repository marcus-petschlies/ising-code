#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>

#define MAIN_PROGRAM
#include "ranlxd.h"
#include "global.h"
#include "geometry.h"
#include "linalg.h"
#include "random.h"
#include "utils.h"
#include "update.h"

/***************************************************************************************
 * Wolff cluster algorithm
 *   - implementation as in Newman & Barkema "Monte Carlo Methods in Statistical Physics",
 *     Oxford University Press
 *
 *   - remember critical temperature for 2-dim. Ising model:  T_c = 2 J / log( 1 + sqrt(2) )
 ***************************************************************************************/

/***************************************************************************************
 *
 ***************************************************************************************/
int main(int argc, char **argv) {

  int c;
  double heat = 0.;
  double J = 0.;
  int Niter = 0;
  int *s = NULL;
  long long int *stack = NULL;
  int seed = 0;
  unsigned int every = 1;
  char LL_str[200] = "NA";

  while ((c = getopt(argc, argv, "N:J:h:L:s:e:v:d:")) != -1) {
    switch (c) {
      case 'd':
        dim = atoi(optarg);
        fprintf(stdout, "# [wolff] dimension set to %u\n", dim );
        break;
      case 'J':
        J = atof(optarg);
        fprintf(stdout, "# [wolff] interaction energy J set to %f\n", J);
        break;
      case 'h':
        heat = atof(optarg);
        fprintf(stdout, "# [wolff] initial heat set o %f\n", heat);
        break;
      case 'L':
        strcpy( LL_str, optarg);
        fprintf(stdout, "# [wolff] lattice size string set to %s\n", LL_str);
        break;
      case 'N':
        Niter = atoi(optarg);
        fprintf(stdout, "# [wolff] number of iterations set to %i\n", Niter);
        break;
      case 's':
        seed = atoi(optarg);
        fprintf(stdout, "# [wolff] seed = %i\n", seed );
        break;
      case 'e':
        every = atoi(optarg);
        fprintf(stdout, "# [wolff] every = %i\n", every );
        break;
      case 'v':
        verbose = atoi(optarg);
        fprintf(stdout, "# [wolff] verbosity level = %d\n", verbose );
        break;
      default:
        exit(1);
    }
  }

  /******************************************************
   * report git version
   ******************************************************/
  fprintf(stdout, "# [wolff] git version = %s\n", gitversion);

  /******************************************************
   * initialize random number generator
   ******************************************************/
  rlxd_init ( 2, seed);

  /******************************************************
   * check lattice size string
   ******************************************************/
  if ( strcmp( LL_str, "NA" ) == 0 ) {
    fprintf ( stderr, "[wolff] Error, LL string\n" );
    exit(2);
  }

  /******************************************************
   * set lattice dimensions
   ******************************************************/
  LL = (unsigned int *)calloc ( dim, sizeof(unsigned int ) );
  char * ptr = strtok ( LL_str, "," );
  if ( ptr == NULL ) exit(3);
  LL[0] = atoi ( ptr );
  for ( unsigned int i = 1; i < dim; i++ ) {
    ptr = strtok ( NULL, "," );
    if ( ptr == NULL ) exit(3);
    LL[i] = atoi ( ptr );
  }

  for ( unsigned int i = 0; i < dim; i++ ) {
    fprintf ( stdout, "# [wolff] LL[%u] = %4u\n", i, LL[i] );
  }

  /******************************************************
   * set VOLUME
   ******************************************************/
  VOLUME = 1;
  for ( unsigned int i = 0; i < dim; i++ ) {
    VOLUME *= LL[i];
  }
  fprintf ( stdout, "# [wolff] VOLUME = %lld\n", VOLUME );

  /******************************************************
   * init nearest neighbour indices
   ******************************************************/
  init_nn_fields ();

  /******************************************************
   * set the acceptance probability
   ******************************************************/
  if ( dim != 2 ) {
    fprintf ( stderr, "# [wolff] Error, no padd for dim != 2 implemented\n" );
    exit(11);
  }
  double const padd = 1 - exp(-2. * J);
  fprintf(stdout, "# [wolff] set P_add = %f\n", padd);

  /******************************************************
   * allocate spin and stack field
   ******************************************************/
  s     = (int*)malloc ( VOLUME * sizeof(int) );
  stack = (long long int*)malloc ( VOLUME * sizeof(long long int));

  if(s == NULL || stack == NULL) {
    fprintf(stderr, "[wolff] Error from malloc\n");
    exit(2);
  }

  /******************************************************
   * initialize the lattice
   ******************************************************/
  for ( long long int i = 0; i < VOLUME; i++ ) {
    s[i] = 2 * (int)(heat * drandom() > 0.5 ) - 1;
  }

  /******************************************************
   * output file
   ******************************************************/
  FILE *ofs = NULL;
  char filename[100];
  sprintf ( filename, "wolf_nd.log" );
  ofs = fopen ( filename, "w" );

  fprintf( ofs, "# J     = %f\n", J );
  fprintf( ofs, "# dim    = %u\n", dim );
  for( unsigned int i = 0; i < dim; i++ ) {
    fprintf( ofs, "# L %u = %u\n", i, LL[i] );
  }
  fprintf( ofs, "# padd  = %f\n", padd );
  fprintf( ofs, "# seed  = %d\n", seed );
  fprintf( ofs, "# heat  = %f\n", heat);
  fprintf( ofs, "# Niter = %u\n", Niter );
  fprintf( ofs, "# every = %u\n", every );

  double const ratime = ((double)clock() / CLOCKS_PER_SEC);

  /******************************************************
   * iterate with the Wolff cluster algorithm
   ******************************************************/
  for ( unsigned int iter = 0; iter < Niter; iter++) {

    wolff_step_periodic ( s, stack, padd );

    /* print_s (s, L, T); */

    if ( ( iter + 1 ) % every == 0 ) {
      int const imeas = ( iter + 1 ) / every;

      int magnetization = 0;
      for ( long long int i = 0; i < VOLUME; i++) {
        magnetization += s[i];
      }
      fprintf ( ofs, "m %8d %25.16e\n", imeas, (double)magnetization / (double)VOLUME );
    }

  }

  double const retime = ((double)clock() / CLOCKS_PER_SEC);
  fprintf ( ofs, "# time = %e seconds\n", retime - ratime );

  fclose ( ofs );

  free(s);
  free(stack);

  fini();
  return(0);
}
