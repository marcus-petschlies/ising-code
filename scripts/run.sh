#!/bin/bash

#HEAT=1
#J=1.


# N=100000

# BETA_C=$(echo "scale=16; 0.4406868 / $J" | bc -l)

#NUM_BETA=100
#BETA_MIN=$(echo "scale=16; 0.80 * $BETA_C" | bc -l)
#BETA_MAX=$(echo "scale=16; 1.25 * $BETA_C" | bc -l)


# for((i=0; i<$NUM_BETA; i++));

# for L in 4 8 16 32 64 128
# for L in 256 512
#for L in 32
#do
#  seed=$(( $L * $L))
#  # BETA=$(echo "scale=16; $BETA_MIN + ($BETA_MAX - $BETA_MIN) * $i / $NUM_BETA" | bc -l)
#  # echo "# [] BETA = $BETA"
#
#  ./wolff -L $L -s $seed -N $N -q 0 1>out.$i 2>err.$i
#
#done

L=32
T=128
seed=387636
every=50
N=10000
# mom=0
J=0.4

./wolff -L $L -T $T -s $seed -N $N -e $every -J $J 1>out 2>err

exit 0
