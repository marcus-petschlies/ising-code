#ifndef _UPDATE_H
#define _UPDATE_H

#ifdef HAVE_OPENMP
#include <omp.h>
#endif
#include "global.h"

void update ( double * const x , double const t, double ** const aux );

void update_hmc (double * const s, double const tau, unsigned int const nmd, double ** const aux );

void update_constrained ( double * const x , double const tau, unsigned const nmd, double ** const aux );

void wolff_step_periodic ( int * const s, long long int * const stack, double const padd );

void wolff_step_helical ( int * const s, long long int * const stack, double padd );

void update_hmc_2 (double * const s, double const tau, unsigned int const nmd, double ** const aux );

#endif
