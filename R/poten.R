fmin <- function(x,m) {
  return( 0.5*m*x^2 - log(cosh(x)) )
}

dfmin <- function(x,m) {
  return( m*x - tanh(x) )
}


poten <- function() {

  m_min <- 0.
  m_max <- 10.
  m_num <- 10000

  xroot_min <- 0
  xroot_max <- 10

  res <- array ( dim = c(m_num + 1, 6)  )

  for ( im in 0:m_num ) {
    m <- m_min + ( m_max - m_min ) / m_num * im

#     uniroot (f=fmin, interval=c(xroot_min, xroot_max), m=m) },
#   u <- tryCatch( { 
##         error   = function(err) { utmp <- list(value=NaN, dvalue=NaN, ddvalue=NaN, tauint=NaN, dtauint=NaN); return(utmp) },
#         warning = function(war) { utmp <- list(value=NaN, dvalue=NaN, ddvalue=NaN, tauint=NaN, dtauint=NaN); return(utmp) },
#         finally = { }
#       )

     u <- optim ( par=0, fn=fmin, gr=dfmin, method=c("Brent"),lower=xroot_min, upper=xroot_max, m=m )

    # res [im+1,] <- c( m, u$root, u$f.root, u$estim.prec )
    res [im+1,] <- c( m, u$par, u$value, u$counts, u$convergence )

  }

  output_filename <- "poten.min.tab"
  cat( "# ", date(), "\n", file=output_filename, append=FALSE )
  write.table ( res, file=output_filename, col.names=FALSE, row.names=FALSE, append=TRUE )

  return ( 0 )

}
