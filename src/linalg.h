#ifndef _LINALG_H
#define _LINALG_H

#ifdef HAVE_OPENMP
#include <omp.h>
#endif
#include "global.h"

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void v_pl_eq_v ( double * const r, double * const s)  {
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    r[i] += s[i];
  }
}  /* end of v_pl_eq_v */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void v_mi_eq_v ( double * const r, double * const s)  {
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    r[i] -= s[i];
  }
}  /* end of v_mi_eq_v */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void v_pl_eq_v_ti_re ( double * const r, double * const s, double const c )  {
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    r[i] += s[i] * c;
  }
}  /* end of v_pl_eq_v_ti_re */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void v_eq_v_pl_v_ti_re ( double * const r, double * const s, double * const t, double const c )  {
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    r[i] = s[i] + t[i] * c;
  }
}  /* end of v_eq_v_pl_v_ti_re */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void v_eq_v ( double * const r, double * const s)  {
  if ( r !=  s ) memcpy ( r, s , VOLUME*sizeof(double) );
}  /* end of v_eq_v */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void v_ti_eq_re ( double * const r, double const c )  {
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    r[i] *= c;
  }
}  /* end of v_ti_eq_re */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void v_eq_re ( double * const r, double const c )  {
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    r[i] = c;
  }
}  /* end of v_eq_re */

/***************************************************************************************
 *
 ***************************************************************************************/
static inline void re_eq_v_dot_v ( double * const c, double * const r, double * const s)  {

  *c = 0.;

#ifdef HAVE_OPENMP
  omp_lock_t writelock;

  omp_init_lock(&writelock);

#pragma omp parallel default(shared) 
{
#endif

  double w2 = 0.;

#ifdef HAVE_OPENMP
#pragma omp for
#endif
  for( long long int ix = 0; ix < VOLUME; ix++ ) {
    w2 += r[ix] * s[ix];
  }
#ifdef HAVE_OPENMP
  omp_set_lock(&writelock);
#endif

  *c += w2;

#ifdef HAVE_OPENMP
  omp_unset_lock(&writelock);
}  /* end of parallel region */
  omp_destroy_lock(&writelock);
#endif

}  /* end of re_eq_v_dot_v */

/***************************************************************************************/
/***************************************************************************************/

/***************************************************************************************
 * sign function on field
 ***************************************************************************************/
static inline void v_eq_sign_v ( double * const r, double * const s ) {
#pragma omp parallel for
  for ( long long int i = 0; i < VOLUME; i++ ) {
    r[i] = (double)( ( s[i] > 0 ) - ( s[i] < 0 ) );
  }
}

/***************************************************************************************/
/***************************************************************************************/

#endif
