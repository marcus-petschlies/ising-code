#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#include "ranlxd.h"

/***************************************************************************************
 * Wolff cluster algorithm
 *   - implementation as in Newman & Barkema "Monte Carlo Methods in Statistical Physics",
 *     Oxford University Press
 *
 *   - remember critical temperature for 2-dim. Ising model:  T_c = 2 J / log( 1 + sqrt(2) )
 ***************************************************************************************/

/***************************************************************************************
 *
 ***************************************************************************************/
inline double drandom(void) {
  double d;
  ranlxd ( &d, 1 );
  return ( d );
}

/***************************************************************************************
 *
 ***************************************************************************************/
void ss_g1 (double _Complex * const c, int const q, int * const s, int const L, int const T ) {

  double const kappa = 2. * M_PI / (double)L * (double)q;

  double _Complex * phase = ( double _Complex *) calloc ( L , sizeof(double _Complex ) );
  double _Complex * s_op  = ( double _Complex *) calloc ( T , sizeof(double _Complex ) );

#pragma omp parallel for
  for ( int x = 0; x < L; x++ ) {
    phase[x] = cexp ( I * kappa * (double)x );
  }

  /* momentum projection */
#pragma omp parallel for
  for ( int t = 0; t < T; t++ ) {
    s_op[t] = 0.;
    for ( int x = 0; x < L; x++ ) {
      int const ix = t * L + x;
      s_op[t] += s[ix] * phase[x];
    }
    s_op[t] /= (double)L;
  }

  free ( phase );
  double _Complex ss_disc = 0.;
  for ( int t = 0; t < T; t++ ) {
    ss_disc += s_op[t];
  }
  ss_disc /= (double)T;

  *c = ss_disc;
  free ( s_op );
}

/***************************************************************************************
 *
 ***************************************************************************************/
void ss_g2 (double _Complex * const c, int const q, int * const s, int const L, int const T ) {

  double const kappa = 2. * M_PI / (double)L * q;

  double _Complex * phase = ( double _Complex *) calloc ( L , sizeof(double _Complex ) );
  double _Complex * s_op  = ( double _Complex *) calloc ( T , sizeof(double _Complex ) );

  for ( int x = 0; x < L; x++ ) {
    phase[x] = cexp ( I * kappa * (double)x );
    /* fprintf ( stdout, "# [ss_g2] phase %3d %16.7e %16.7e\n", x, creal(phase[x]), cimag(phase[x] ) ); */
  }

  /* momentum projection */
  for ( int t = 0; t < T; t++ ) {
    s_op[t] = 0.;
    for ( int x = 0; x < L; x++ ) {
      int const ix = t * L + x;
      s_op[t] += s[ix] * phase[x];
    }
    s_op[t] /= (double)L;
  }

  free ( phase );

  /* time-difference */
  for ( int dt = 0; dt < T; dt++ ) {
    c[dt] = 0.;
    /* offset */
    for ( int t = 0; t < T; t++ ) {
      int const tt = ( t + dt + T ) % T;
      c[dt] += conj(s_op[t]) * s_op[tt];
    }
    c[dt] /= (double)T;
  }

/*

  for ( int dt = 0; dt < T; dt++ ) {
    c[dt] -= ss_disc;
  }
*/
  free ( s_op );

}

/***************************************************************************************
 *
 ***************************************************************************************/
inline void print_s (int*s, int const L, int const T) {

  int i, k, ik;

  ik = 0;
  for(i=0; i<T; i++) {
    for(k=0; k<L; k++) {
      if(s[ik] == 1)  {
        fprintf(stdout, "x|");
      } else {
        fprintf(stdout, "o|");
      }
      ik++;
    }
    fprintf(stdout, "\n");
    for(k=0; k<2*L; k++) {
      fprintf(stdout, "-");
    }
    fprintf(stdout, "\n");
  }
  fprintf(stdout, "\n");

}


/***************************************************************************************
 *
 ***************************************************************************************/
void step_pbc (int*s, int*stack, double padd, int const L, int const T) {

  int i;
  int sp;
  int oldspin, newspin;
  int current, nn;
  int N = T*L;
  int t, x;

  i = (int) ( N * drandom() );

  stack[0] = i;
  sp = 1;
  oldspin =  s[i];
  newspin = -s[i];

  s[i] = newspin;

  while (sp) {

    current = stack[--sp];

    t = current / L;
    x = current % L;


    /* check the neighbours */
    nn = t * L + ( x + 1 ) % L;
    if(s[nn] == oldspin) {
      if( drandom() < padd ) {
        stack[sp++] = nn;
        s[nn] = newspin;
      }
    }

    nn = t * L + ( x - 1 + L ) % L;
    if(s[nn] == oldspin) {
      if( drandom() < padd ) {
        stack[sp++] = nn;
        s[nn] = newspin;
      }
    }

    nn = ( ( t + 1 ) % T ) * L + x;
    if(s[nn] == oldspin) {
      if( drandom() < padd ) {
        stack[sp++] = nn;
        s[nn] = newspin;
      }
    }

    nn = ( ( t - 1 + T ) % T ) * L + x;
    if(s[nn] == oldspin) {
      if( drandom() < padd ) {
        stack[sp++] = nn;
        s[nn] = newspin;
      }
    }

  /* fprintf(stdout, "# [step] i = %d sp = %d\n", i, sp); */

  }  /* end of while sp */

}  /* end of step_pbc */


/***************************************************************************************
 *
 ***************************************************************************************/
void step (int*s, int*stack, double padd, int const L, int const T) {

  int i;
  int sp;
  int oldspin, newspin;
  int current, nn;
  int N = T*L;
  int XNN = 1;
  int YNN = L;

  i = (int) ( N * drandom() );


  stack[0] = i;
  sp = 1;
  oldspin =  s[i];
  newspin = -s[i];

  while (sp) {

    current = stack[--sp];

    /* check the neighbours */
    if ( (nn = current + XNN) >= N ) nn -= N;
    if(s[nn] == oldspin) {
      if( drandom() < padd ) {
        stack[sp++] = nn;
        s[nn] = newspin;
      }
    }

    if ( (nn = current - XNN) < 0 ) nn += N;
    if(s[nn] == oldspin) {
      if( drandom() < padd ) {
        stack[sp++] = nn;
        s[nn] = newspin;
      }
    }

    if ( (nn = current + YNN) >= N ) nn -= N;
    if(s[nn] == oldspin) {
      if( drandom() < padd ) {
        stack[sp++] = nn;
        s[nn] = newspin;
      }
    }

    if ( (nn = current - YNN) < 0 ) nn += N;
    if(s[nn] == oldspin) {
      if( drandom() < padd ) {
        stack[sp++] = nn;
        s[nn] = newspin;
      }
    }

  /* fprintf(stdout, "# [step] i = %d sp = %d\n", i, sp); */

  }  /* end of while sp */

}  /* end of step */

/***************************************************************************************/
/***************************************************************************************/

/***************************************************************************************
 *
 ***************************************************************************************/
int main(int argc, char **argv) {

  int c;
  int L = 0;
  int T = 0;
  double beta = 1.;               /* default use coupling J as proxy for J/T = J beta */
  double heat = 0.;               /* heat for initial spin configuration */
  double J = 0.4406867935097715;  /* critical value */
  int Niter = 0;
  int *s = NULL, *stack = NULL;
  int seed = 0;
  int mom = 0;
  int every = 1;

  while ((c = getopt(argc, argv, "b:N:J:h:L:T:s:q:e:")) != -1) {
    switch (c) {
      case 'b':
        beta = atof(optarg);
        fprintf(stdout, "# [wolff] inverse temperature set to %f\n", beta);
        break;
      case 'J':
        J = atof(optarg);
        fprintf(stdout, "# [wolff] interaction energy J set to %f\n", J);
        break;
      case 'h':
        heat = atof(optarg);
        fprintf(stdout, "# [wolff] initial heat set o %f\n", heat);
        break;
      case 'N':
        Niter = atoi(optarg);
        fprintf(stdout, "# [wolff] number of iterations set to %i\n", Niter);
        break;
      case 'L':
        L = atoi(optarg);
        fprintf(stdout, "# [wolff] lattice size L set to %i\n", L);
        break;
      case 'T':
        T = atoi(optarg);
        fprintf(stdout, "# [wolff] lattice size T set to %i\n", T);
        break;
      case 's':
        seed = atoi(optarg);
        fprintf(stdout, "# [wolff] seed = %i\n", seed );
        break;
      case 'q':
        mom = atoi(optarg);
        fprintf(stdout, "# [wolff] mom = %i\n", mom );
        break;
      case 'e':
        every = atoi(optarg);
        fprintf(stdout, "# [wolff] every = %i\n", every );
        break;
      default:
        exit(1);
    }
  }

  /* initialize random number generator */
  rlxd_init ( 2, seed);

  /* set total number of spins */
  int const N = L * T;

  /* set the acceptance probability */
  double const padd = 1 - exp(-2.*beta*J);
  fprintf(stdout, "# [wolff] set P_add = %f\n", padd);

  /* allocate spin and stack field */
  s = (int*)malloc(N * sizeof(int));
  stack = (int*)malloc(N * sizeof(int));

  if(s == NULL || stack == NULL) {
    fprintf(stderr, "[wolff] Error from malloc\n");
    exit(2);
  }

  /* initialize the lattice */
  if(heat == 0.) {
    for ( int i = 0; i < N; i++) {
      s[i] = 1;
    }
  } else if (heat == 1.) {
    for ( int i = 0; i < N; i++) {
      s[i] =  drandom() > 0.5 ? 1 : -1;
    }
  }

  /* output file */
  FILE *ofs = NULL;
  char filename[100];
  /* sprintf ( filename, "T%d_L%d_q%d_wm.ssc", T, L, mom ); */
  sprintf ( filename, "T%d_L%d_wm.ssc", T, L );
  ofs = fopen ( filename, "w" );

  fprintf( ofs, "# beta  = %f\n", beta );
  fprintf( ofs, "# J     = %f\n", J );
  fprintf( ofs, "# T     = %d\n", T );
  fprintf( ofs, "# L     = %d\n", L );
  fprintf( ofs, "# padd  = %f\n", padd );
  fprintf( ofs, "# seed  = %d\n", seed );
  fprintf( ofs, "# heat  = %f\n", heat);
  fprintf( ofs, "# Niter = %d\n", Niter );

#if 0
  FILE *ofs2=NULL;
  /* sprintf ( filename, "T%d_L%d_q%d_wm.ssd", T, L, mom ); */
  sprintf ( filename, "T%d_L%d_wm.ssd", T, L );
  ofs2 = fopen ( filename, "w" );

  fprintf( ofs, "# beta  = %f\n", beta );
  fprintf( ofs, "# J     = %f\n", J );
  fprintf( ofs, "# T     = %d\n", T );
  fprintf( ofs, "# L     = %d\n", L );
  fprintf( ofs, "# padd  = %f\n", padd );
  fprintf( ofs, "# seed  = %d\n", seed );
  fprintf( ofs, "# heat  = %f\n", heat);
  fprintf( ofs, "# Niter = %d\n", Niter );
#endif
  double const ratime = ((double)clock() / CLOCKS_PER_SEC);

#if 0
  double _Complex * ssc = (double _Complex *) malloc ( T * sizeof(double _Complex) );
  double _Complex ssd = 0.;
#endif

  /* iterate with the Wolff cluster algorithm */
  for ( int iter = 0; iter < Niter; iter++) {
    step_pbc(s, stack, padd, L, T);

    int magnetization = 0;

    for ( int i = 0; i < N; i++) {
      magnetization += s[i];
    }
    fprintf(ofs, "m %8d %25.16e\n", iter, (double)magnetization / (double)N);
    /* print_s (s, L, T); */

#if 0
    if ( ( iter + 1 ) % every == 0 ) {
      int const imeas = ( iter + 1 ) / every;
      for ( int imom = 0; imom < L; imom++ ) {
        ss_g1 ( &ssd, imom, s, L, T );
        ss_g2 (  ssc, imom, s, L, T );
        for( int k = 0; k < T; k++ ) {
          fprintf ( ofs, "%10d %10d %4d %6d %25.16e %25.16e\n", iter, imeas, imom, k, creal( ssc[k] ), cimag( ssc[k] ) );
        }
        fprintf(ofs2, "%10d %10d %4d %16.7e %16.7e\n", iter, imeas, imom, creal( ssd ), cimag( ssd ) );
      }
    }
#endif
  }

  double const retime = ((double)clock() / CLOCKS_PER_SEC);
  fprintf ( ofs, "# time = %e seconds\n", retime - ratime );

  fclose ( ofs );
  /* fclose ( ofs2 ); */

  free(s);
  /* free ( ssc ); */
  free(stack);
  return(0);
}
